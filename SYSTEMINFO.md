# Incognito

Vendor: Incognito
Homepage: https://www.incognito.com/

Product: Incognito
Product Page: https://www.incognito.com/

## Introduction
We classify Incognito into the Data Center and Network Services domains as Incognito provides network-related services and functionalities.

## Why Integrate
The Incognito adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Incognito. With this adapter you have the ability to perform operation on items such as:

- Device
- IP
- Subnet
- VPN

## Additional Product Documentation
The [API documents for Incognito]()