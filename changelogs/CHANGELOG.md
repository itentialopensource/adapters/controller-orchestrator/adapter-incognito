
## 0.2.2 [06-14-2023]

* Update healthcheck endpoint

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!4

---

## 0.2.1 [06-07-2023]

* set up sso port for login

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!3

---

## 0.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!2

---

## 0.1.2 [06-30-2021]

- The token comes back in authentication not authorization

See merge request itentialopensource/adapters/certified-integration-documents/controller-orchestrator/adapter-incognito!1

---

## 0.1.1 [05-13-2021]

- Initial Commit

See commit 3d4f0d8

---
