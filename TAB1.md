# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Incognito System. The API that was used to build the adapter for Incognito is usually available in the report directory of this adapter. The adapter utilizes the Incognito API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Incognito adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Incognito. With this adapter you have the ability to perform operation on items such as:

- Device
- IP
- Subnet
- VPN

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
