
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:33PM

See merge request itentialopensource/adapters/adapter-incognito!17

---

## 0.4.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-incognito!15

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:46PM

See merge request itentialopensource/adapters/adapter-incognito!14

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:00PM

See merge request itentialopensource/adapters/adapter-incognito!13

---

## 0.4.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!12

---

## 0.3.6 [03-28-2024]

* Changes made at 2024.03.28_13:40PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!11

---

## 0.3.5 [03-11-2024]

* Changes made at 2024.03.11_15:58PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!10

---

## 0.3.4 [02-28-2024]

* Changes made at 2024.02.28_11:26AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!9

---

## 0.3.3 [12-24-2023]

* update metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!8

---

## 0.3.2 [12-20-2023]

* update axios

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!7

---

## 0.3.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!6

---

## 0.3.0 [11-08-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!5

---

## 0.2.2 [06-14-2023]

* Update healthcheck endpoint

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!4

---

## 0.2.1 [06-07-2023]

* set up sso port for login

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!3

---

## 0.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito!2

---

## 0.1.2 [06-30-2021]

- The token comes back in authentication not authorization

See merge request itentialopensource/adapters/certified-integration-documents/controller-orchestrator/adapter-incognito!1

---

## 0.1.1 [05-13-2021]

- Initial Commit

See commit 3d4f0d8

---
